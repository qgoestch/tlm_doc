scene\_generation package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scene_generation.materials
   scene_generation.objects
   scene_generation.parts

Submodules
----------

scene\_generation.document module
---------------------------------

.. automodule:: scene_generation.document
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.gen\_disp module
----------------------------------

.. automodule:: scene_generation.gen_disp
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.gen\_script module
------------------------------------

.. automodule:: scene_generation.gen_script
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.gen\_test\_objects module
-------------------------------------------

.. automodule:: scene_generation.gen_test_objects
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: scene_generation
   :members:
   :undoc-members:
   :show-inheritance:
