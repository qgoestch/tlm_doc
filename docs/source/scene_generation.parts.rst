scene\_generation.parts package
===============================

Submodules
----------

scene\_generation.parts.box module
----------------------------------

.. automodule:: scene_generation.parts.box
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.parts.cone module
-----------------------------------

.. automodule:: scene_generation.parts.cone
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.parts.cylinder module
---------------------------------------

.. automodule:: scene_generation.parts.cylinder
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.parts.main\_part module
-----------------------------------------

.. automodule:: scene_generation.parts.main_part
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.parts.plane module
------------------------------------

.. automodule:: scene_generation.parts.plane
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.parts.sphere module
-------------------------------------

.. automodule:: scene_generation.parts.sphere
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: scene_generation.parts
   :members:
   :undoc-members:
   :show-inheritance:
