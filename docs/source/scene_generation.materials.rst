scene\_generation.materials package
===================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scene_generation.materials.wood

Submodules
----------

scene\_generation.materials.main\_material module
-------------------------------------------------

.. automodule:: scene_generation.materials.main_material
   :members:
   :undoc-members:
   :show-inheritance:

scene\_generation.materials.raw\_material module
------------------------------------------------

.. automodule:: scene_generation.materials.raw_material
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: scene_generation.materials
   :members:
   :undoc-members:
   :show-inheritance:
