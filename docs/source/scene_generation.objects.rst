scene\_generation.objects package
=================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scene_generation.objects.tree

Submodules
----------

scene\_generation.objects.base\_object module
---------------------------------------------

.. automodule:: scene_generation.objects.base_object
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: scene_generation.objects
   :members:
   :undoc-members:
   :show-inheritance:
