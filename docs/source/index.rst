
Welcome to TLM_python's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   readme
   modules

Features
--------

- TLM Python code for 3D wave propagation.
- Forest test cases

Installation
------------

Install from Github

.. code-block:: text

    git clone https://github.com/qgoestch/TLM_python.git
    cd TLM_python
    pip install .


Contribute
----------

- Issue Tracker: github.com/$project/$project/issues
- Source Code: github.com/$project/$project

Support
-------

If you are having issues, please let us know.

License
-------

The project is licensed under the BSD license.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

