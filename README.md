# TLM_python
## Description
___
TLM Python code for 3D wave propagation


## Setup
___
See the following file for [installation instructions](./setup.md).


## Example
___
See the following file for an [example to run a simulation](./example.md).

## Modules
___
- [FastVoxel](./FastVoxel/README.md)
- [LibSimpa](./libsimpa/README.md)
- [SceneGeneration](./scene_generation/README.md)
- [TLMCore](./tlm_core/README.md)
- [Tools](./tools/README.md)
- [VectfitPython](./vectfit_python/README.md)
