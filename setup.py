from setuptools import setup, find_packages

setup(
    name='TLM_python',
    version='0.1.0',
    packages=find_packages(include=['tools', 'altvoxel', 'libsimpa', 'tlm_core', 'fastvoxel', 'vectfit_python', 'scene_generation', 'scene_generation.parts']),
    install_requires=[
        'alabaster',
        'breathe',
        'Sphinx',
        'sphinx-rtd-theme',
        'sphinx-sitemap',
        'sphinxcontrib-applehelp',
        'sphinxcontrib-devhelp',
        'sphinxcontrib-htmlhelp',
        'sphinxcontrib-jsmath',
        'sphinxcontrib-qthelp',
        'sphinxcontrib-serializinghtml',
        ],
    url='',
    license='',
    author='qgoestch',
    author_email='quentin.goestchel@cerema.fr',
    description=''
)

